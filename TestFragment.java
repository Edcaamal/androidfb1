package com.example.fbuacamnuevo1;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fbuacamnuevo1.model.Usuarios;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TestFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TestFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    // TODO: Configuración inicial del Main
    // private StorageReference mStorageRef;
    // Write a message to the database
    // FirebaseDatabase database = FirebaseDatabase.getInstance();
    //DatabaseReference myRef = database.getReference("DemoUac");
    public  String URL_INTERNET = "https://static4.abc.es/media/play/2018/04/03/kiko-chavo-ocho-kTnH--620x349@abc.jpg";
    private static final String URL_INTERNET_PICASO = "http://i.imgur.com/DvpvklR.png";

    private DatabaseReference myRef, myApp;
    private ImageView IVLoadInternet;

    EditText txtId, txtIdFB, txtNombre, txtEmail, txtSexo, txtEdad;
    TextView tvMyApp;
    Button buttonEntradaFB;


    public TestFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TestFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TestFragment newInstance(String param1, String param2) {
        TestFragment fragment = new TestFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
   }

     @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_test, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Inflate the layout for this fragment
        // cargaOriginal();
        //  mStorageRef = FirebaseStorage.getInstance().getReference();
        // cargaObjetoUsuario();

        // FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        myRef = FirebaseDatabase.getInstance().getReference("DemoUac");
        txtId     = view.findViewById(R.id.etId);
        txtNombre = view.findViewById(R.id.etNombre);
        txtEmail  = view.findViewById(R.id.etEmail);
        txtSexo   = view.findViewById(R.id.etSexo);
        txtEdad   = view.findViewById(R.id.etEdad);
        tvMyApp   = view.findViewById(R.id.tvMyApp);
        buttonEntradaFB = view.findViewById(R.id.btnEntrada);
        // android:onClick="entradaFB">

        myApp     = FirebaseDatabase.getInstance().getReference("UacPruebas");

        SetUpView(view);

        myApp.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String titulo = dataSnapshot.getValue(String.class);
                //tvMyApp.setText(titulo);
                URL_INTERNET = titulo;
                LoadImageByInterneUrlWithPicasso();
                Log.i("addValueEventListener", titulo);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });
        buttonEntradaFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entradaFB(v);
            }
        });

    }

    /*  Código Erroneo
        private void SetUpView() {
            IVLoadInternet = findViewById(R.id.tvMyApp);
        }

        private void LoadImageByInterneUrlWithPicasso(){
            picaso.get()
                    .load(URL_INTERNET)
                    .into(IVLoadInternet);
        }

     */
    private void SetUpView(View view) {
        IVLoadInternet = view.findViewById(R.id.ivMyApp);  // tvMyApp => ivMyApp
    }

    private void LoadImageByInterneUrlWithPicasso() {
        // picaso => Picasso. Importar Class
        Picasso.get()
                .load(URL_INTERNET)
                .into(IVLoadInternet);
    }



    public void entradaFB(View view){
        Usuarios usuario;
        int id        = Integer.parseInt(txtId.getText().toString());
        String nombre = txtNombre.getText().toString();
        String email  = txtEmail.getText().toString();
        String sexo   = txtSexo.getText().toString();
        int edad      = Integer.parseInt(txtEdad.getText().toString());
        Date dFInsert= new Date();
        Log.i("entradFB", " "+dFInsert);

        usuario = new Usuarios(id, "1", nombre, email, sexo, edad, dFInsert);
        Log.i("entradaFB", usuario.toString());
        myRef.child("Alumnos").push().setValue(usuario);

    }

    private void cargaObjetoUsuario() {
        Usuarios usuario;
        Date dFInsert= new Date();
        usuario = new Usuarios(1, "1", "Jose Luis", "correo@mail.com", "M", 24, dFInsert );
        myRef.child("Usuarios").push().setValue(usuario);

        usuario = new Usuarios(2, "1", "Ulises", "correo1@mail.com", "M", 22, dFInsert );
        myRef.child("Usuarios").push().setValue(usuario);
        usuario = new Usuarios(3, "1", "Sergio", "correo2@mail.com", "M", 25, dFInsert );
        myRef.child("Usuarios").push().setValue(usuario);


    }

    private void cargaOriginal(){
        //Log.i("onCreate", database.toString());
        Log.i("onCreate", myRef.toString());

        myRef.child("Usuarios").child("DBA").setValue("Ulises");
        myRef.child("Usuarios").child("Developer").setValue("Sergio");
        myRef.child("Usuarios").child("Designer").setValue("Thelma");
        myRef.child("Usuarios").child("Operator").setValue("Jose Luis");

        myRef.child("Articulos").child("Lapiz").setValue(10.00);
        myRef.child("Articulos").child("Pluma").setValue(15.00);
        myRef.child("Articulos").child("Cuaderno").setValue(35.00);
        myRef.child("Articulos").child("Borrador").setValue(5.00);

        myRef.child("Clientes").child("Ulises").setValue("Ocasional");
        myRef.child("Clientes").child("Sergio").setValue("Frecuente");
        myRef.child("Clientes").child("Thelma").setValue("Unico");
        myRef.child("Clientes").child("Jose Luis").setValue("Plus");

        myRef.child("Clientes").push().child("Jose Luis").setValue("Plus");
        myRef.child("Clientes").push().child("Ulises").setValue("Ocasional");
        myRef.child("Clientes").push().child("Sergio").setValue("Frecuente");
        myRef.child("Clientes").push().child("Thelma").setValue("Unico");
        myRef.child("Clientes").push().child("Jose Luis").setValue("Plus");
        myRef.child("Clientes").push().child("Jose Luis").setValue("Plus");
        myRef.child("Clientes").push().child("Ulises").setValue("Ocasional");
        myRef.child("Clientes").push().child("Sergio").setValue("Frecuente");
        myRef.child("Clientes").push().child("Thelma").setValue("Unico");
        myRef.child("Clientes").push().child("Jose Luis").setValue("Plus");

    }


}