package com.example.fbecaamal1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {
    // TODO: Variables propias
    private Button btnSignIn;
    private SignInButton       btnSignInGoogle;

    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 123;
   // private static final NavController navController;
    private NavController navControllerLogin;
    // Valiable para verificar el el login de Google
    private FirebaseAuth mAuth;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    // Método para validar si el usuario ya esta logeado no realizar el proceso de solicitar conexion
    // Abrir el fragmento que sigue

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null){
            // TODO: Ir al siguiente fragmento
            Log.i("Log In Google", user.getEmail());
            navControllerLogin.navigate(R.id.action_loginFragment_to_inicioFragment);
        }

    }



    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //final NavController navControllerLogin = Navigation.findNavController(view);
        navControllerLogin = Navigation.findNavController(view);
        // 1.6 mAuth inicializar
        mAuth = FirebaseAuth.getInstance();

        btnSignInGoogle = (SignInButton) view.findViewById(R.id.btnSignInGoogle);
        Inicialize(view);
        btnSignInGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();

            }
        });

    }

    // 1.2 Crear método de Inicialize, Importar librerias
    private void Inicialize(View view){
        //Configurar Google SignIn
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // Construir un GoogleSignInClient [Activity : this, Fragmanet: getActivity]
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);

    }

    // 1.3 Crear signIn, Importar Intent, crear variable local RC_SIGN_IN asignarle un valor de identificación
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    // 1.4  Importar class [GoogleSignInAccount, Task, ApiException, Log]
    // Crear método firebaseAuthWithGoogle, copiar de la Documenación
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
                Log.w("Login Google", "Google sign in OK ");
                // TODO: Ir al siguiente fragmento ...


            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w("Login Google", "Google sign in failed", e);
                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                // ...
            }
        }
    }

    // 1.5     TAG: firebase Auth
    // Comentar updateUI(user), método no creado
    // Comentar Log
    // Importar AuthCredential, GoogleAuthProvider, AuthResult, OnCompleteListener, FirebaseUser
    // Comentar Snakbar
    // mAuth more actions, create field
    //     private FirebaseAuth mAuth
    //     modificar el contexto para el fragment
    //     .addOnCompleteListener(this,    .addOnCompleteListener(getActivity()
    //     1.6 mAuth inicializar
    //         mAuth = FirebaseAuth.getInstance();
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        //Log.d("firebase Auth", "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
         mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            // Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            // TODO: Ir al siguiente fragmento
                            navControllerLogin.navigate(R.id.action_loginFragment_to_inicioFragment);
                            // updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("firebase Auth", "signInWithCredential:failure", task.getException());
                            // Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            // updateUI(null);
                        }

                        // ...
                    }
                });
    }
}