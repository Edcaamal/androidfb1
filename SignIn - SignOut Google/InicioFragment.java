package com.example.fbecaamal1;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.EmailAuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link InicioFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InicioFragment extends Fragment {
    public  String URL_INTERNET = "http://i.imgur.com/DvpvklR.png";
    private static final String URL_INTERNET_PICASO = "http://i.imgur.com/DvpvklR.png";

    // TODO: Variables propias
    CardView cvTest, cvLogout;
    TextView txtTitle, txtSubTitle;
    // Logout Firebase, Google
    ImageView ivImgGoogle;
    FirebaseAuth myUser;
    private GoogleSignInClient mGoogleSignInClient;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    public InicioFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InicioFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InicioFragment newInstance(String param1, String param2) {
        InicioFragment fragment = new InicioFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_inicio, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final NavController navController = Navigation.findNavController(view);
        cvTest      = view.findViewById(R.id.cvInicio_Test);
        cvLogout    = view.findViewById(R.id.cvLogout);
        txtSubTitle = view.findViewById(R.id.textSubTitle);
        ivImgGoogle = view.findViewById(R.id.ivImgGoogle);

       // Inicializar librerias de Picasso y cuenta de Google
        SetUpView(view);
        Inicialize(view);
        GoogleSignInAccount signInAccount = GoogleSignIn.getLastSignedInAccount(getActivity());
        if (signInAccount != null){
            // Mostrar correo, nombre e imagen de usuario de google
            txtSubTitle.setText(signInAccount.getEmail()+"-"+signInAccount.getDisplayName());
            URL_INTERNET = signInAccount.getPhotoUrl().toString();
            Log.i("ImgGoogle", URL_INTERNET);
            LoadImageByInterneUrlWithPicasso();
        }
        //
        cvTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.action_inicioFragment_to_testFragment);

            }
        });
        cvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGoogleSignInClient.signOut();

                myUser = FirebaseAuth.getInstance();
                //mAuth = FirebaseAuth.getInstance();
                Log.i("Log Out Google", myUser.getCurrentUser().getEmail());
                myUser.signOut();
                navController.navigate(R.id.action_inicioFragment_to_loginFragment);

            }
        });
    }
    // 1.2 Crear método de Inicialize, Importar librerias
    private void Inicialize(View view){
        //Configurar Google SignIn
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // Construir un GoogleSignInClient [Activity : this, Fragmanet: getActivity]
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);

    }

    // Manejo de imagenes con Picasso
    private void SetUpView(View view) {
        ivImgGoogle = view.findViewById(R.id.ivImgGoogle);  // tvMyApp => ivMyApp
    }

    private void LoadImageByInterneUrlWithPicasso() {
        // picaso => Picasso. Importar Class
        Picasso.get()
                .load(URL_INTERNET)
                .into(ivImgGoogle);
    }
}