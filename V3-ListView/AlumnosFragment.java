package com.example.fbuacamnuevo1;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.example.fbuacamnuevo1.model.Usuarios;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AlumnosFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AlumnosFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private DatabaseReference  myRef;
    TextInputLayout txtIdAlumno,  txtNombreAlumno, txtEmailAlumno, txtSexoAlumno, txtEdadAlumno;
    Switch swSexoAlumno;
    Button buttonIngresarAlumno;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AlumnosFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AlumnosFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AlumnosFragment newInstance(String param1, String param2) {
        AlumnosFragment fragment = new AlumnosFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_alumnos, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        myRef = FirebaseDatabase.getInstance().getReference("Demo_Uac");
        txtIdAlumno          = view.findViewById(R.id.txtIdAlumno);
        txtNombreAlumno      = view.findViewById(R.id.txtNombreAlumno);
        txtEmailAlumno       = view.findViewById(R.id.txtEmailAlumno);
//        txtSexoAlumno        = view.findViewById(R.id.swSexoAlumno);
        swSexoAlumno         = view.findViewById(R.id.swSexoAlumno);
        txtEdadAlumno        = view.findViewById(R.id.txtEdadAlumno);
        buttonIngresarAlumno = view.findViewById(R.id.btnIngresarAlumno);

        buttonIngresarAlumno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IngresarAlumno(v);
            }
        });

    }

    public void IngresarAlumno(View view){
        Usuarios usuario;
        // Integer.parseInt(counterMaxTextField.getEditText().getText().toString());
        int id        = Integer.parseInt(txtIdAlumno.getEditText().getText().toString());
        String nombre = txtNombreAlumno.getEditText().getText().toString();
        String email  = txtEmailAlumno.getEditText().getText().toString();
        String sexo;
        sexo = (!swSexoAlumno.isChecked())? "Hombre": "Mujer";
        //String sexo = "H";
        int edad      = Integer.parseInt(txtEdadAlumno.getEditText().getText().toString());
        Date dFInsert   = new Date();
        String sFInsert = dFInsert.toString();
        Log.i("entradFB", " "+dFInsert);

        usuario = new Usuarios(id, sFInsert, nombre, email, sexo, edad, dFInsert);
        Log.i("entradaFB", usuario.toString());
        myRef.child("Alumnos").push().setValue(usuario);



    }


}