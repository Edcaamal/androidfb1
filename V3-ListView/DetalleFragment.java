package com.example.fbuacamnuevo1;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fbuacamnuevo1.model.Usuarios;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetalleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetalleFragment extends Fragment {
    // TODO: Declaraciones propias
    private DatabaseReference myRefDetalle;
    ImageView myimageView;
    TextView  textTitulo, textSubtitulo, textDetalle;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Variables propias
    TextView myTitleDetalle;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public DetalleFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetalleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetalleFragment newInstance(String param1, String param2) {
        DetalleFragment fragment = new DetalleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_detalle, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myTitleDetalle    = (TextView) view.findViewById(R.id.tvTitleDetalle);
        myimageView       = (ImageView) view.findViewById(R.id.ivImageDetalle);
        textTitulo        = (TextView) view.findViewById(R.id.txtTitulo);
        textSubtitulo     = (TextView) view.findViewById(R.id.txtSubTitulo);
        textDetalle       = (TextView) view.findViewById(R.id.txtDetalles);

        if (getArguments() != null){
            Toast.makeText(getActivity(), "Pasando Argumentos entre Fragments " , Toast.LENGTH_SHORT).show();
            DetalleFragmentArgs args = DetalleFragmentArgs.fromBundle(getArguments());
            final String myRefFB   = args.getMyRefFB();
            final String myKeyFB   = args.getMyKeyIDFB();

            String myValueFB = args.getMyValueFB();
            myTitleDetalle.setText("Detalle => "+myRefFB+" -> "+myKeyFB+" = "+myValueFB);
            Log.v("onViewCreated",myRefFB+" -> "+myKeyFB+" = "+myValueFB );

            myRefDetalle = FirebaseDatabase.getInstance().getReference().child(myRefFB).child(myKeyFB);
            myRefDetalle.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String Titulo = "", Subtitulo ="", Detalle = "";
                    Log.v("onDataChange",myRefDetalle.toString() );
                    Log.v("onDataChange",myRefFB );
                    if (dataSnapshot.exists()){
                        if (myRefFB.equals("Test")){
                            Titulo     = myRefFB;
                            Subtitulo  = myKeyFB;
                            Detalle     = dataSnapshot.getValue().toString();
                            //Log.v("onDataChange",snapshot.getValue().toString() );
                        }else{
                           // for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                                // Obtener un objeto
                            Log.v("onDataChange", "Obterner nodo");
                            Usuarios detalle = dataSnapshot.getValue(Usuarios.class);

                            if (detalle.getSexo().equals("Mujer")){
                                myimageView.setImageResource(R.drawable.ic_student);
                            }else{
                                if (detalle.getEdad()>0 && detalle.getEdad() <30 ){
                                    myimageView.setImageResource(R.drawable.ico_student_man);
                                }else{
                                    myimageView.setImageResource(R.drawable.ic_teacher_writing);
                                }
                            }
                                 Titulo    = detalle.getNombre();
                                Subtitulo = detalle.getEmail();
                                Detalle   = "Sexo = "+detalle.getSexo()+" Edad = "+detalle.getEdad();
                                Log.v("onDataChange", detalle.toString());
                           // }
                        }
                    }

                    textTitulo.setText(Titulo);
                    textSubtitulo.setText(Subtitulo);
                    textDetalle.setText(Detalle);


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });




        }
    }
}