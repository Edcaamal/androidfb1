package com.example.fbuacamnuevo1;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fbuacamnuevo1.model.Usuarios;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MaestrosFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MaestrosFragment extends Fragment {
    // Declaraciones propias
    private DatabaseReference myRef;
    ListView myListView;

    ArrayList<String> myArrayList = new ArrayList<>();
    ArrayList<String> myKeyList   = new ArrayList<>();
    private String NodoConsulta;
    TextView myTitle;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public MaestrosFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MaestrosFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MaestrosFragment newInstance(String param1, String param2) {
        MaestrosFragment fragment = new MaestrosFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_maestros, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Añadir el Control Navigator (import)
        final NavController navController = Navigation.findNavController(view);

        final ArrayAdapter<String> myArrayAdapter= new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, myArrayList);

        myListView = (ListView) view.findViewById(R.id.lvMaestros);
        myTitle    = (TextView) view.findViewById(R.id.tvTitleMaestros);
        myListView.setAdapter(myArrayAdapter);
        // NodoConsulta = "Test";
         NodoConsulta = "Demo_Uac/Alumnos";
        myTitle.setText("ListView: " + NodoConsulta);

        Log.i("onViewCreated", "Inicial");


        myRef = FirebaseDatabase.getInstance().getReference().child(NodoConsulta);

        //Escuchar si hay cambios en la DB,  actualizar la lista y notificar al adaptador
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                myArrayList.clear();
                myKeyList.clear();
                Log.v("onDataChange","Cambio en la DB" );
                if (dataSnapshot.exists()){
                    Log.v("onDataChange","dataSnapShot.exist" );

                    for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                        myKeyList.add(snapshot.getKey());  //Añadimos al arreglo los nombres de los nodos
                        // Obtener un solo "valor"
                        if (NodoConsulta.equals("Test")){
                            myArrayList.add(snapshot.getValue().toString());
                            Log.v("onDataChange",snapshot.getValue().toString() );

                        }else{
                            // Obtener un objeto
                            Usuarios alumnos = snapshot.getValue(Usuarios.class);
                            String   cadena  = alumnos.getNombre()+" "+ alumnos.getEmail();

                            myArrayList.add(cadena);
                            Log.v("onDataChange", cadena);
                        }
                    }
                        
                }else{
                    Toast.makeText(getActivity(), "No se encontraron registros y/o no existe el Nodo", Toast.LENGTH_SHORT).show();
                }
                myArrayAdapter.notifyDataSetChanged();
             }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        myListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                String clavenodo = myArrayList.get(position);
                String keynodo   = myKeyList.get(position);
                Log.v("setOnItemLongClickListener clavenodo", clavenodo);
                Log.v("setOnItemLongClickListener keynodo", keynodo);

                if (NodoConsulta.equals("Test")){
                    myArrayList.remove(position);
                    myRef.child(keynodo).removeValue();
                } else{
                    myArrayList.remove(position);
                    myRef.child(keynodo).removeValue();
                }

                myArrayAdapter.notifyDataSetChanged();
                return false;
            }
        });

       //
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String clavenodo = myArrayList.get(position);
                String keynodo   = myKeyList.get(position);
                Log.v("setOnItemLongClickListener clavenodo", clavenodo);
                Log.v("setOnItemLongClickListener keynodo", keynodo);
                Toast.makeText(getActivity(), "Click en el Nodo: "+keynodo+"-"+clavenodo , Toast.LENGTH_SHORT).show();

                // Primera Version Solo Seguimiento de funciones
                //navController.navigate(R.id.action_maestrosFragment_to_detalleFragment);

                // Segunda Version Maestro Detalle
                MaestrosFragmentDirections.ActionMaestrosFragmentToDetalleFragment action = MaestrosFragmentDirections.actionMaestrosFragmentToDetalleFragment(NodoConsulta, keynodo, clavenodo) ;
                navController.navigate(action);

            }
        });


        /*
        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Log.i("onViewCreated", "onChildAddeddataSnapshot");
                //  1era Version
                String value = dataSnapshot.getValue(String.class);
                myArrayList.add(value);
                myArrayAdapter.notifyDataSetChanged();


            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                String value = dataSnapshot.getValue(String.class);
                Log.i("onViewCreated", "onChildChanged");
                myArrayList.add(value);
                myArrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                Log.i("onViewCreated", "onChildChanged");
                myArrayList.remove(value);
                myArrayAdapter.notifyDataSetChanged();

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
*/

    }
}