package com.example.fbuacamnuevo1.model;

import java.sql.Timestamp;
import java.util.Date;

public class Usuarios {
    private int     id;
    private String idFB;
    private String nombre;
    private String email;
    private String sexo;
    private int    edad;
    private Date fInsert;

    public Usuarios() {
    }

    public Usuarios(int id, String idFB, String nombre, String email, String sexo, int edad, Date fInsert) {
        this.id = id;
        this.idFB = idFB;
        this.nombre = nombre;
        this.email = email;
        this.sexo = sexo;
        this.edad = edad;
        this.fInsert = fInsert;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdFB() {
        return idFB;
    }

    public void setIdFB(String idFB) {
        this.idFB = idFB;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Date getfInsert() {
        return fInsert;
    }

/*    public void setfInsert(String fInsert) {
        this.fInsert = fInsert;
    }

 */

    public void setfInsert(Date fInsert) {
        this.fInsert = fInsert;
    }

    @Override
    public String toString() {
        return "Usuarios{" +
                "id=" + id +
                ", idFB='" + idFB + '\'' +
                ", nombre='" + nombre + '\'' +
                ", email='" + email + '\'' +
                ", sexo='" + sexo + '\'' +
                ", edad=" + edad +
                ", fInsert='" + fInsert + '\'' +
                '}';
    }
}
